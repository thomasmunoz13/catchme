Catch me if you want #SWAG edition
==========
## By Anthony.L, Thomas.M, Loïc.P, Dylan.P

The rules are simple : 
You must catch your opponent before he catch you.
You can play with or without a real person (if you don't have any friends, that's ok, 
we will not judge you).
You can also play with your cat.

Enjoy ! 