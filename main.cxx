
/*!

@file main.cxx
@author Loic P., Thomas M., Anthony L., Dylan P.
@date 12 janvier 2014
@version  1.0
@brief Jeu catch me if you can
 
@mainpage Documention for the game "Catch me if you can" 
*/

#include <iostream>
#include <istream>
#include <sstream>
#include <string.h>
#include <vector>
#include <algorithm>
#include <stdio.h>
#include <ctype.h>
#include <fstream>
#include <stdlib.h>
#include <utility>
#include <time.h>
#include <unistd.h>
#include <termios.h>

using namespace std;


/*!
@typedef CVLine
@brief This typedef is to define a vector of char
@callergraph
*/
typedef vector <char> CVLine; 

/*!
@typedef CMatrix
@brief This typedef is to define a matrix of char.@n
        Is a vector of vector of char by contraction with @p CVLine typedef. 
*/
typedef vector <CVLine> CMatrix;

/*!
@typedef CPosition
@brief This define a @p pair of two @p unsigned. We use it for the position
        of player.
*/
typedef pair   <unsigned, unsigned> CPosition; 



/*!
@namespace NSCatch
            This namespace contain all the functions the game needs. @n These functions can only be used on the software.
*/
namespace  NSCatch
{
    /*!
    @fn void control()
    @brief This function have for goal to check if the standard input is good and if it's fail, @p control flush the buffer of
            standard input and clear the errors bytes on cin.
    */
    void control()
    {
        if (cin.fail())
        {
            cout << "\nInvalid choice !\n";
            cin.clear();
            cin.ignore( numeric_limits<streamsize>::max(), '\n');
        }
    }//control()

    /*!
    @var Touches
    @brief This vector of char contain the current touch for move the Token of player or anything which can move in game.
    */
    vector <char> Touches = {'a','z','e','q','d','w','x','c','p'};

    /*!
    @var KTokenPlayer1
    @brief The token
           @p KTokenPlayer1 is a const char who define the Player 1 in the game. 
    @callergraph
    @callgraph
    */
    const char KTokenPlayer1 = 'X';

   /*!
    @var KTokenPlayer2
    @brief The token
           @p KTokenPlayer2 is a const char who define the Player 2 in the game. 
    */
    const char KTokenPlayer2 = 'O';

    /*!
    @var KEmpty
    @brief The token
           @p KEmpty is a const char only use for initialize the matrix. 
    */
    const char KEmpty        = ' ';

    /*!
    @var KTokenObstacle
    @brief The token
           @p KTokenObstacle is a const char who define the obstacle in game. 
    */
    const char KTokenObstacle = '#';
    /*!
    @var KTokenBonusTourDouble
    @brief This token is use to indicate to the player this is a Bonus.
    */ 
    const char KTokenBonusTourDouble = '+';

    const string KReset   = "0";
    const string KNoir    = "30";
    const string KRouge   = "31";
    const string KVert    = "32";
    const string KJaune   = "33";
    const string KBleu    = "34";
    const string KMAgenta = "35";
    const string KCyan    = "36";

    void ClearScreen ()
    {
        cout << "\033[H\033[2J";
    }//CLearScreen()
    
    /*! @fn unsigned TestF(const string & File)
      @brief This function is used to test if the file in parameter exist in the folder
     @param[in] File is the string which contain the name of file
     @return The return is a unsigned 1 if the file doesn't exist, otherwise it is 0
    */
    unsigned TestF(const string & File)
    {
        ifstream ifs(File); // we try to open the file
        if (!ifs.is_open()) // we test if the file is open
        {
            return 1; // If is not open return 1
        }
        return 0;   // Else return 0
    }//TestF

    /*!
    @fn getch()
    @brief This function has been copy at the URL http://stackoverflow.com/a/912796 . It's for make capture key from standard 
    input without press enter.
    @return char This is character. 
    */
    char getch() {
        char buf = 0;
        struct termios old;
        if (tcgetattr(0, &old) < 0)
                perror("tcsetattr()");
        old.c_lflag &= ~ICANON;
        old.c_lflag &= ~ECHO;
        old.c_cc[VMIN] = 1;
        old.c_cc[VTIME] = 0;
        if (tcsetattr(0, TCSANOW, &old) < 0)
                perror("tcsetattr ICANON");
        if (read(0, &buf, 1) < 0)
                perror ("read()");
        old.c_lflag |= ICANON;
        old.c_lflag |= ECHO;
        if (tcsetattr(0, TCSADRAIN, &old) < 0)
                perror ("tcsetattr ~ICANON");
        return (buf);
    }//getch()
    /*!
    @fn void Couleur (const string & coul)
    @parm[in] coul The color display on the screen
    @brief This function changes the color of font.
    */
    void Couleur (const string & coul)
    {
        cout << "\033[" << coul << "m";
    }//Couleur()

    /*!
    @fn void EcriLog(const string& filename)
    @brief This function is for write the name of file, in the Log.txt.
    @param[in] filename It's the name of file we want to write in the Log file
    @return No return because it's a void function.
    */ 
    void EcriLog(const string& FileName)
    {
        ofstream ofs("Log.txt",ios_base::out|ios_base::app); // We open the log file in output
        if (!ofs.is_open())
        {
            cout <<"\nFile not found or access denied"; // if the file is not open we print an error message.
            return;
        }
        ofs << FileName <<"\n"; // If the log file is open we write the file name on this.
    }//EcrirLog

    /*!
    @fn void Unexist(const string& File)
    @param [in] File This string contain the file we want to create (Here only the Log or config file)
    @brief This functions was call if the file in parameter doesn't exist and the function create this file for have no error, another time.
    */
    void Unexist(const string& File)
    {
        while(true)
        {
            cout << "\nNo file found";
            cout << "\nDo you want to create a file for the next time ?(y/n)";

            char Ans;
            cin >> Ans; // we capture the answer of the user.
            Ans=tolower(Ans);

            if(cin.eof()) break; //If is EOF the function break.
            else if(isdigit(Ans))
            {
                cout << "\nInvalid choice\n";
                cin.clear();
                cin.ignore( numeric_limits<streamsize>::max(), '\n');
                continue;
            }
            
            if(Ans == 'y') //if the answer is y the function create the file. 
            {
                ofstream ofs(File);
                if(File == "config.txt")
                {
                    ofs << "NbLine: 10" <<"\n"
                        << "NbCol: 10" <<"\n"
                        << "XPosPlay1: 0"<<"\n"
                        << "YPosPlay1: 9"<<"\n"
                        << "XPosPlay2: 9"<<"\n"
                        << "YPosPlay2: 0"<<"\n"; 
                }
                return;
            }
            else if (Ans == 'n') // else if ans is n the function break
            {
                return;
            }
            else // else the value of ans is not good and the function erase the buffer, and continue.
            {
                cout << "\nInvalid choice\n";
                cin.clear();
                cin.ignore( numeric_limits<streamsize>::max(), '\n');
                continue;
            }
        }
    }//Unexist()

    /*!
    @fn void LireLog(string& Fichier)
    @brief This function have for goal, to read the Log file and print on 
    screen the file present in this. After you can choose a file, and if
    doesn't exist, the functions erease it of file, and you can chose another
    file.
    @param[out] choix Is a @p int parameter for choice made by the user.
    @param[out] Fichier This @p string paramater is to return the name of 
                file choose by the user.
    */
    void LireLog(string& Fichier)
    {
        ClearScreen(); //Clear the screen
        cout << "Here the files in the log file : \n";
        unsigned Choix;
        vector <string> VFichier;
        string Line;
        
        while(true)
        {
            int count = 0;
            ifstream ifs("Log.txt",ios_base::in); //open the log file
            
            if (!ifs.is_open()) 
            {
                Unexist("Log.txt"); // if the file is not open, the function call Unexist which will create the file.
            }
            
            if(cin.eof()) break; // Control and catch if an EOF is enter.
            if(ifs.eof()) break; // Control if the ifs is not end.
            while(getline(ifs,Line)) // Capture the line per line the content of ifs.
            {
                cout <<"\n " << count << " ";
                cout << Line; //Print the content.
                VFichier.push_back(Line);
                ++count;
            }
            if(count == 0) // If no file int log print an error message.
            {
                cout << "No file available \n";
				break;
            }
            cout << "\nYour choice :"; //Ask the choice of user.
            cin >> Choix;
            if(cin.eof()) break; //Control the input.
            control();

            Fichier = VFichier[Choix];
            unsigned Present;
            
            Present = TestF(Fichier); //Test if the file is available or not
            if(Present == 1) // If the file is no available we delete this file on log file.
            {
                cout << "\nFile not found";
                cout << "\nPress a key to continue : ";

                ifs.close();
                ofstream ofs("Log.txt",ios_base::out|ios_base::trunc);
                unsigned i = 0;
                while(i < Fichier.size())
                {
                    if(VFichier[i] != Fichier)
                    {
                       ofs << Fichier[i] << '\n';
                    }
                    ++i;
                }
                --i;
                
                VFichier.erase(VFichier.end()-1);
                continue;
            }
            else // else we break.
                break;
        }
    }//LireLog()

    /*!
        @fn int menu()
        @brief This function just print on screen a menu, to make a choice.
        @return This function return a @p int for the choice of the user. 
    */
    int menu()
    {
        unsigned Choice;
        while(Choice != 4) //We print the menu.
        {    
            cout << "\nChoose a mode :\n";
            cout << "1.An obstacle\n";
            cout << "2.Position of Player 1\n";
            cout << "3.Position of Player 2\n";
            cout << "4.Quit\n";
            cout << "What is your choice ? ";
                
            cin >> Choice; //Ask for the answer.
            if (cin.eof()) break; // control of standard input 
            control();
            break;
        }
        return Choice; // we return the choice of the user.
    }//menu()

    /*!
        @fn unsigned Rand (unsigned Min, unsigned Max)
        @brief This function just choose a random number between the range.
        @param [in] Min It's the minimal range
        @param [in] Max It's the maxiaml range
        @return @p Rand return the random number it's an @p unsigned
    */
    unsigned Rand (unsigned Min, unsigned Max)
    {
        unsigned Res;
        Res = rand() % (Max - Min) + Min;
        return Res;
    }//Rand()

    /*!
        @fn void SaveFirst(const string & FileName, unsigned Colonnes, unsigned Line)
        @brief The function @p SaveFirst is only use for save the number of line and columns
        in a text file.
        @param [in] Filename It's the name of file, in which @p SaveFirst write the number
        of line and columns
        @param [in] col It's the numbuer of columns
        @param [in] Line Is the numbuer of lines
     */
    void SaveFirst(const string & FileName, unsigned Colonnes, unsigned Line)
    {
        ofstream ofs;
        ofs.open(FileName,ios::out|ios::trunc); //Open in output the file and trunc the contents.
        
        if(!ofs.is_open()) //Check if the file is open.
        {

            cout << "Save file not found or access denied\n";
            cout << "Press any key to quit";
            getch();
            
            return;
        }
        
        ofs << "NbLine: " << Line << '\n' << "NbCol: " << Colonnes << '\n'; // If is open we insert the numbers of line and columns into this file.
        ofs.close();
    }//SaveFirst()

    /*!
        @fn void SaveEd(CMatrix & Mat, const string & FileName)
        @brief @p SaveEd Save all the matrix content in a text file.
        @param [in] Mat Is the matrix we want to save in a file.
        @param [in] FileName Is the name of the file where we 
        want to save the matrix
    */
    void SaveEd(CMatrix & Mat, const string & FileName)
    {
        ofstream ofs;
        ofs.open(FileName,ios::out|ios::app);//Open the file in output with the position of cursor at the end of prensent data.
        
        if(!ofs.is_open()) // check if file is open.
        {

            cout << "Save file not found or access denied\n";
            cout << "Press any key to quit";
            getch();
            return;
        }
        
        for(unsigned i = 0; i < Mat.size(); ++i) //for each case of matrix we check the current character and make a switch on this to determinate the value.
        {
            for(CVLine::size_type j(0);j < Mat[i].size();++j)
            {
                char Cour = Mat[i][j];
                
                switch (Cour)
                {
                        
                    case KTokenPlayer1 : //If is the Token of player 1 we apply this output.
                    {
                        ofs << "XPosPlay1: " << i << '\n' << "YPosPlay1: " << j << '\n';
                        break;
                    }
                    case KTokenPlayer2 : //Is the same way like Token of player 1.
                    {
                        ofs << "XPosPlay2: " << i << '\n' << "YPosPlay2: " << j << '\n';
                        break;
                    }
                    case KTokenObstacle:
                    {
                        ofs << "XPosObs: " << i << '\n' << "YPosObs: " << j << '\n';
                        break;
                    }
                        
                }
            }
            
        }
        ofs.close(); // And we close the file.
    }//SaveEd()

    /*!
    @fn void Save(CMatrix& Mat, const string& FileName, bool Bot, unsigned Difficulty)
    @brief @p Save , save all the content of a matrix, with the difficulty, if the 
    the bot is activated or not. In order continue a game.
    @param [in] Mat This is the matrix we want to save.
    @param [in] FileName This is the name of file where we want to save the matrix.
    @param [in] Bot This parameter is used to know if the bot is on or off.
    @param [in] Difficulty Is used to know the difficulty choose by the user.
    */
    void Save(CMatrix& Mat, const string& FileName, bool Bot, unsigned Difficulty)
    {
        ofstream ofs(FileName,ios_base::out|ios_base::trunc); //Open the file.
        
        if(!ofs.is_open()) //Check if the file is open.
        {
            cout << "Save file not found or access denied\n";
        }
        
        if(Bot) //Check if the bot is true or not.
            ofs << "Bot: true" << '\n';
        else
            ofs << "Bot: false" << '\n';
        
        ofs << "Difficulty: " << Difficulty << '\n'; //insert difficulty 
        
        ofs << "NbLine: " << Mat[0].size() << '\n' << "NbCol: " << Mat.size() << '\n'; //Insert Size of matrix
        
        for(unsigned i = 0; i < Mat.size(); ++i) // Here check each case of matrix and based on the contents of the box we insert on the file the appropriate sentence.
        {
            for(CVLine::size_type j(0);j < Mat[i].size();++j)
            {
                char Cour = Mat[i][j];
                
                switch (Cour)
                {
                        
                    case KTokenPlayer1 :
                    {
                        ofs << "XPosPlay1: " << i << '\n' << "YPosPlay1: " << j << '\n';
                        break;
                    }
                    case KTokenPlayer2 :
                    {
                        ofs << "XPosPlay2: " << i << '\n' << "YPosPlay2: " << j << '\n';
                        break;
                    }
                    case KTokenObstacle:
                    {
                        ofs << "XPosObs: " << i << '\n' << "YPosObs: " << j << '\n';
                        break;
                    }
                        
                }
            }
            
        }
    }//Save()

    /*!
      @fn void ObstacleGenerator (CMatrix& Matrix, unsigned Difficulty)
      @brief This function creates a variable number of obstacle according to the Difficulty.
      @param[out] Matrix This is the Matrix of the game
      @param[in] Difficulty This is the value of the difficulty of the game
     */
   void ObstacleGenerator(CMatrix& Matrix, unsigned Difficulty)
    {
        srand (unsigned(time(NULL))); //initialize seed
        
        unsigned NbObstacle = 0;
        
        switch(Difficulty)
        {
            case 1 :
                NbObstacle = unsigned(( (Matrix.size() * Matrix[0].size()) / 10));
                break;
                
            case 2 :
                NbObstacle = unsigned(( (Matrix.size() * 3) + (Matrix.size() % 2 ) )/2);
                break;
        }
        
        unsigned Line;
        unsigned Column;
        
        for(unsigned i = 0; i < NbObstacle; ++i)
        {
            do //create a random position for the obstacle
            {
                Line = Rand(0,unsigned(Matrix.size()));
                Column = Rand(0, unsigned(Matrix[0].size()));
                
            }
            while(Matrix[Line][Column] == KTokenPlayer1 || Matrix[Line][Column] == KTokenPlayer2);
            
            Matrix[Line][Column] = KTokenObstacle;
        }
        
    }//ObstacleGenerator()

    /*!
    @fn void BonusGenerator(CMatrix& Matrix, unsigned Difficulty)
    @brief This function asks to the player what is the difficulty of the game
    then, he apply the choice withe ObstacleGenerator() by creating bonuses
    in the Matrix.
    @param[out] Matrix This is the Matrix of the game
    @param[in] Difficulty This is the value of the difficulty of the game
    */
    void BonusGenerator(CMatrix& Matrix, unsigned Difficulty)
    {
        unsigned NbBonus = 0; //numbers of total bonus

        switch(Difficulty) //two different quantities of bonus according to the difficulty
        {
            case 1 :
                NbBonus = unsigned(( (Matrix.size() * Matrix[0].size()) / 20));
                break;

            case 2 :
                NbBonus = unsigned(( (Matrix.size() * Matrix[0].size()) / 30));
        }

        unsigned Line;
        unsigned Column;

        for(unsigned i=0; i<NbBonus; ++i) 
        {
            do //create a random position for the bonus
            {
                Line = Rand(0,unsigned(Matrix.size()));
                Column = Rand(0, unsigned(Matrix[0].size()));
                
            }
            while(Matrix[Line][Column] == KTokenPlayer1 || Matrix[Line][Column] == KTokenPlayer2); //check if the position is different to an obstacle if it' s different, it places the bonus
            {
                if (Matrix[Line][Column] != KTokenObstacle)
                {
                    Matrix[Line][Column] = KTokenBonusTourDouble;
                }
            }
        }
    }//BonusGenerator

    /*!
    @fn void DifficultyChoice (CMatrix& Matrix, unsigned& Difficulty)
    @param [out] Matrix Is used to modify the matrix
    @param [out] Difficulty Is used to save the difficulty choice by the user
    @brief This function print on standard outpout a little menu where the user can chose the difficulty he wants for the game.
    */
    void DifficultyChoice (CMatrix& Matrix, unsigned& Difficulty)
    {
       unsigned Choice; //initilaze the choice of the user.
       
       while(true)
       {
			cout << "Choose a difficulty level (0 for easy, 1 for hard, 2 for extreme) : "; // print the question on standars output
			cin >> Choice; // Capture the answer
			
			if(cin.eof()) //constrol the input
				break;
				
			else if(cin.fail())
			{	
				control();
				continue;
			}
			
			if(Choice > 2)
			{
				cout << "Input error !\n";
                cin.ignore(numeric_limits<streamsize>::max(), '\n');
                continue;
			}
			else
				break;
        
		}
		
        Difficulty = Choice;
        ObstacleGenerator(Matrix, Choice);
        BonusGenerator(Matrix, Choice);
        
    }//DifficultyChoice()

    /*!
    @fn    void InitMat (CMatrix & Mat, unsigned NbLine, unsigned NbColumn, CPosition Player1, CPosition Player2, bool Virgin = false)
    @brief The function @p InitMat is use for create an empty @p char matrix whit the character @p KEmpty
    @param[out] Mat This is the matrix we want initialize/create.
    @param[in] NbLine This is the number of line we want to have for the matrix.
    @param[in] NbColumn This is the number of columns we want to have for the matrix.
    @param[in] Player 1 This is the position of player 1.
    @param[in] Player 2 This is the position of player 2.
    @param[in] Virgin This says if the function is used by Editor or for the game.
    */
    void InitMat (CMatrix & Mat, unsigned NbLine, unsigned NbColumn, CPosition Player1, CPosition Player2, bool Virgin = false)
    {
        Mat.resize(NbLine); //Here we initilaze the matrix whit the number of line
        
        for(unsigned i = 0; i < NbLine; ++i) // For each case of line we resize of number of column.
        {
            Mat[i].resize(NbColumn);
            
            for(unsigned j = 0; j < NbColumn; ++j)
            {
                Mat[i][j] = KEmpty;
            }
        }
        if(!Virgin) // If the virgin is false the function place the 2 Position of players ,if not they don't place the player.  
        {
			Mat[Player1.first][Player1.second] = KTokenPlayer1;
			Mat[Player2.first][Player2.second] = KTokenPlayer2;
		}
           
    }//InitMat()

    /*!
    @fn ShowMatrix (const CMatrix& Mat)
    @brief This function show the matrix place in parameter, it's change of background color at each case, and color the two player token and bonus in different colors.
    @param [in] Mat This the matrix we want to show.
    */
    void  ShowMatrix (const CMatrix& Mat)
    {
        
        ClearScreen();

        for (unsigned i =0; i < Mat.size(); ++i) 
        {
            cout << "\n";
            for(char Val : Mat[i]) //for each case of the matrix.
            {

                cout << "\033[47m"; //We color the background of the terminal.
                cout << " | ";
                
                if (Val == KTokenPlayer1) //If the current case contain this value, so we color it on different color.
                {
                    Couleur(KRouge);
                    cout << Val;
                    Couleur(KReset);
                }
                else if(Val == KTokenPlayer2)// They work on the way like Token1
                {
                    Couleur(KJaune);
                    cout << Val;
                    Couleur(KReset);
                }
                else if(Val == KTokenBonusTourDouble)
                {
                    Couleur(KVert);
                    cout << Val;
                    Couleur(KReset);
                }
                else
                {
                    cout << Val;
                }
            }
            
        }
        cout << "\033[0m"; // Here we restore the inital color.
        cout << "\n";
    }//ShowMatrix()

    /*!
    @fn MoveToken(CMatrix& Matrix, char Move,unsigned& End , CPosition& Player, const vector<char>& Touch = Touches, const char& Edit = 'n')
    @param [out] Matrix This parameter is the matrix on the which we want to make the movement.
    @param [in] Move It is the movement that the user wants to make.
    @param [out] Player This is the position where the player is, and therefore that the selected move will apply.
    @param [in] Touch These are the keys used in the current portion to move.
    @param [in] Edit This variable is used to determine which part of the program should be launched either party for games or for editing.
    @brief The function @p MoveToken is used to move it in the set parameter of the current position, to the movement chosen.
    */
    void MoveToken(CMatrix& Matrix, char Move,unsigned& End , CPosition& Player,vector<char> Touch = Touches,char Edit = 'n')
    {
        char car = Matrix[Player.first][Player.second];
        
        Matrix[Player.first][Player.second]=KEmpty;
        
        if(Edit == 'n')
        {
            if(Move == Touch[0])
            {
                if(Player.second > 0 && Player.first > 0 && (Matrix[Player.first - 1][Player.second - 1] != KTokenObstacle))
                {
                    --Player.first;
                    --Player.second;
                    End = 0;
                }
                else
                    End = 1;
            }

            else if (Move == Touch[1])
            {
                if(Player.first > 0 && (Matrix[Player.first - 1][Player.second] != KTokenObstacle))
                {
                    --Player.first;
                    End = 0;
                }
                else
                    End = 1;
            }

            else if (Move == Touch[2])
            {
                if(Player.second < (Matrix.size()-1) && Player.first > 0 && (Matrix[Player.first - 1][Player.second + 1] != KTokenObstacle))
                {
                    --Player.first;
                    ++Player.second;
                    End = 0;
                }
                else
                    End = 1;
            }

            else if (Move == Touch[3])
            {
                if (Player.second > 0 && (Matrix[Player.first][Player.second - 1] != KTokenObstacle))
                {
                    --Player.second;
                    End = 0;
                }
                else
                    End = 1;
            }

            else if (Move == Touch[4])
            {
                if (Player.second < (Matrix[0].size()-1) && (Matrix[Player.first][Player.second + 1] != KTokenObstacle))
                {
                    ++Player.second;
                    End = 0;
                }
                else
                    End = 1;
            }

            else if (Move == Touch[5])
            {
                if(Player.second > 0 && Player.first < Matrix.size()-1 && (Matrix[Player.first + 1][Player.second - 1] != KTokenObstacle))
                {
                    ++Player.first;
                    --Player.second;
                    End = 0;
                }
                else
                    End = 1;
            }

            else if (Move == Touch[6])
            {
                if (Player.first < (Matrix.size() - 1) && (Matrix[Player.first + 1][Player.second] != KTokenObstacle))
                {End =
                    ++Player.first;
                    End = 0;
                }
                else
                    End = 1;
            }
                
            else if (Move == Touch[7])
            {
                if(Player.second < (Matrix.size()-1) && Player.first < Matrix.size()-1 && (Matrix[Player.first + 1][Player.second + 1] != KTokenObstacle))
                {
                    ++Player.first;
                    ++Player.second;
                    End = 0;
                }
                else
                    End = 1;
            }
        }
        else if(Edit == 'y') // If the function is called by the editor, we change the controls and apply following controls.
        {
          if(Move == Touch[0])
            {
                if(Player.second > 0 && Player.first > 0 && (Matrix[Player.first - 1][Player.second - 1] == ' '))
                {
                    --Player.first;
                    --Player.second;
                    End = 0;
                }
                else
                    End = 1;
            }

            else if (Move == Touch[1])
            {
                if(Player.first > 0 && (Matrix[Player.first - 1][Player.second] == ' '))
                {
                    --Player.first;
                    End = 0;
                }
                else
                    End = 1;
            }

            else if (Move == Touch[2])
            {
                if(Player.second < (Matrix.size()-1) && Player.first > 0 && (Matrix[Player.first - 1][Player.second + 1] == ' '))
                {
                    --Player.first;
                    ++Player.second;
                    End = 0;
                }
                else
                    End = 1;
            }

            else if (Move == Touch[3])
            {
                if (Player.second > 0 && (Matrix[Player.first][Player.second - 1] == ' '))
                {
                    --Player.second;
                    End = 0;
                }
                else
                    End = 1;
            }

            else if (Move == Touch[4])
            {
                if (Player.second < (Matrix[0].size()-1) && (Matrix[Player.first][Player.second + 1] == ' '))
                {
                    ++Player.second;
                    End = 0;
                }
                else
                    End = 1;
            }

            else if (Move == Touch[5])
            {
                if(Player.second > 0 && Player.first < Matrix.size()-1 && (Matrix[Player.first + 1][Player.second - 1] == ' '))
                {
                    ++Player.first;
                    --Player.second;
                    End = 0;
                }
                else
                   End = 1;
            }

            else if (Move == Touch[6])
            {
                if (Player.first < (Matrix.size() - 1) && (Matrix[Player.first + 1][Player.second] == ' '))
                {
                    ++Player.first;
                   End = 0;
                }
                else
                   End = 1;
            }
                
            else if (Move == Touch[7])
            {
                if(Player.second < (Matrix.size()-1) && Player.first < Matrix.size()-1 && (Matrix[Player.first + 1][Player.second + 1] == ' '))
                {
                    ++Player.first;
                    ++Player.second;
                   End = 0;
                }
                else
                   End = 1;
            } 
        }
        
        
        Matrix[Player.first][Player.second] = car;
        
    }//MoveToken()

     /*!
      @fn void AvoidObs(CMatrix& Matrix, char Move, CPosition& BotPos,const vector<char>& Touch= Touches)
      @brief This function permit the IA to avoid obstacles (when the difficulty is 2)
      @param[out] Matrix This is the Matrix where we want to do the movement
      @param[in] Move This is the movement we initially want to do
      @param[out] BotPos This is the position of the Bot
      */
    void AvoidObs(CMatrix& Matrix, char Move, CPosition& BotPos,const vector<char>& Touch= Touches)
    {
       //This function avoid the obstacle if there is one, otherwise, it executes the move choosen.
        
            unsigned UnUsed;
            if(Move =='s')
                MoveToken(Matrix, Move, UnUsed , BotPos);
                
            else if(Move == Touch[1])
            {
                if(Matrix[BotPos.first-1][BotPos.second] != KTokenObstacle)
                    MoveToken(Matrix, Move,UnUsed , BotPos);
                    
                else if(Matrix[BotPos.first-1][BotPos.second+1] != KTokenObstacle)
                    MoveToken(Matrix, Touch[2],UnUsed , BotPos);
                    
                else
                    MoveToken(Matrix, Touch[0],UnUsed , BotPos);
             }
            else if(Move == Touch[3])
            {
                if(Matrix[BotPos.first][BotPos.second-1] != KTokenObstacle)
                    MoveToken(Matrix, Move,UnUsed , BotPos);
                    
                else if(Matrix[BotPos.first-1][BotPos.second-1] != KTokenObstacle)
                    MoveToken(Matrix, Touch[0], UnUsed, BotPos);
                    
                else
                    MoveToken(Matrix, Touch[5],UnUsed , BotPos);
            }
            else if(Move == Touch[6])
            {
                if(Matrix[BotPos.first+1][BotPos.second] != KTokenObstacle)
                    MoveToken(Matrix, Move, UnUsed,  BotPos);
                    
                else if(Matrix[BotPos.first+1][BotPos.second-1] != KTokenObstacle)
                    MoveToken(Matrix, Touch[5], UnUsed, BotPos);
                    
                else
                    MoveToken(Matrix, Touch[7], UnUsed, BotPos);
            }
            else if(Move == Touch[4])
            {
                if(Matrix[BotPos.first][BotPos.second+1] != KTokenObstacle)
                    MoveToken(Matrix, Move, UnUsed, BotPos);
                    
                else if(Matrix[BotPos.first-1][BotPos.second+1] != KTokenObstacle)
                    MoveToken(Matrix, Touch[2], UnUsed, BotPos);
                    
                else
                    MoveToken(Matrix, Touch[7], UnUsed, BotPos);
            }
            else if(Move == Touch[2])
            {
                if(Matrix[BotPos.first-1][BotPos.second+1] != KTokenObstacle)
                    MoveToken(Matrix, Move, UnUsed, BotPos);
                    
                else if(Matrix[BotPos.first-1][BotPos.second] != KTokenObstacle)
                    MoveToken(Matrix, Touch[1], UnUsed, BotPos);
                    
                else
                    MoveToken(Matrix, Touch[4], UnUsed, BotPos);
            }
            else if(Move == Touch[0])
            {
                if(Matrix[BotPos.first-1][BotPos.second-1] != KTokenObstacle)
                    MoveToken(Matrix, Move, UnUsed, BotPos);
                    
                else if(Matrix[BotPos.first-1][BotPos.second] != KTokenObstacle)
                    MoveToken(Matrix, Touch[1], UnUsed, BotPos);
                    
                else
                    MoveToken(Matrix, Touch[3], UnUsed, BotPos);
            }
            else if(Move == Touch[5])
            {
                if(Matrix[BotPos.first+1][BotPos.second-1] != KTokenObstacle)
                    MoveToken(Matrix, Move, UnUsed, BotPos);
                    
                else if(Matrix[BotPos.first+1][BotPos.second] != KTokenObstacle)
                    MoveToken(Matrix, Touch[6], UnUsed, BotPos);
                    
                else
                    MoveToken(Matrix, Touch[3], UnUsed, BotPos);
            }
            else if(Move == Touch[7])
            {
                if(Matrix[BotPos.first+1][BotPos.second+1] != KTokenObstacle)
                    MoveToken(Matrix, Move, UnUsed, BotPos);
                    
                else if(Matrix[BotPos.first+1][BotPos.second] != KTokenObstacle)
                    MoveToken(Matrix, Touch[6], UnUsed, BotPos);
                    
                else
                    MoveToken(Matrix, Touch[4], UnUsed, BotPos);
            }
    }//AvoidObs()

    /*!
      @fn void IA(CMatrix& Matrix, CPosition Player1, CPosition& BotPos, unsigned Difficulty,const vector<char>& Touch = Touches)
      @brief This function chose the best move for the IA depending on the difficulty
      @param[out] Matrix This is the Matrix of the game
      @param[out] Player1 This is the position of the Player 1 
      @param[out] BotPos This is the position of the Bot
      @param[in] Difficulty This is the difficulty of the game (because the bot's moves are differents depending on the difficulty)
      */
    void IA(CMatrix& Matrix, CPosition Player1, CPosition& BotPos, unsigned Difficulty,const vector<char>& Touch = Touches)
    {
        char Move;
        unsigned NumberMove;
        unsigned NoUse;
        
        if(Difficulty < 2)
        {
            NumberMove = Rand(0,9);
            
            switch(NumberMove)
            {
                case 0:
                    Move = Touch[0];
                    break;
                case 1:
                    Move = Touch[1];
                    break;
                case 2:
                    Move = Touch[2];
                    break;
                case 3:
                    Move = Touch[3];
                    break;
                case 4:
                    Move = 's';
                    break;
                case 5:
                    Move = Touch[4];
                    break;
                case 6:
                    Move = Touch[5];
                    break;
                case 7:
                    Move = Touch[6];
                    break;
                case 8:
                    Move = Touch[7];
                    break;
                case 9:
                    Move = 's';
                    break;
            }
        }
        else
        {
            if(Player1.first < BotPos.first)
            {
                if(Player1.second == BotPos.second)
                    Move = Touch[1];
            
                else if(Player1.second < BotPos.second)
                    Move = Touch[0];
            
                else
                    Move = Touch[2];
            }
        
            else if(Player1.first == BotPos.first)
            {
                if(Player1.second > BotPos.second)
                    Move = Touch[4];
                else
                    Move = Touch[3];
            }
        
            else
            {
                if(Player1.second == BotPos.second)
                    Move = Touch[6];
            
                else if(Player1.second > BotPos.second)
                    Move = Touch[7];
                else
                    Move = Touch[5];
            }
        }
        if(Difficulty > 1)
            AvoidObs(Matrix, Move, BotPos);
        else
            MoveToken(Matrix, Move, NoUse, BotPos);
            
    }//IA()

    /*!
    @fn void InitMatFromFile (CMatrix & Mat, const string & FileName, CPosition & Player1, CPosition & Player2, vector<CPosition>& ObsPos, bool& Bot, unsigned& Difficulty)
    @brief This function is for init a matrix with a file.
    @param [out] Mat This the matrix we want to initialaze.
    @param [in] This is the file we want to use to initialaze the matrix.
    @param [in] Player1  Is the position of the player 1.
    @param [in] Player1  Is the position of the player 2.
    @param [out] ObsPos This is the vector who contain all the positions of vector.
    @param [out] Bot Is this a boolean to know if the IA is activated or not.
    @param [out] Difficulty This infroms about the difficulty choose by the user 
    */
    void InitMatFromFile (CMatrix & Mat, const string & FileName, CPosition & Player1, CPosition & Player2, vector<CPosition>& ObsPos, bool& Bot, unsigned& Difficulty)
    {
        ifstream ifs(FileName,ios_base::in);
        if(!ifs.is_open())
        {
            if(FileName == "config.txt") //if the config file doesn't exist we create it and initialize a new amtrix.
            {
                cout << "Configuration file not found !\n";
                Unexist("config.txt");
                
                InitMat(Mat, 10, 10, Player1, Player2);
                Player1.first = 0;
                Player1.second = 9;
                Player2.first = 9;
                Player2.second = 0;
                
                Mat[Player1.first][Player1.second] = KTokenPlayer1;
                Mat[Player2.first][Player2.second] = KTokenPlayer2;
                Mat[0][0]=KEmpty;
            }
            else
            { 
                cout << "Save file not found !\n";
            }
            return;
        }
        
        CVLine::size_type NbLine;
        CVLine::size_type NbColone;
        string Botstr;
        string LigneCour;
        unsigned CptObs = 0;
        
        while(ifs >> LigneCour)
        {
            if(LigneCour == "NbLine:")
            {
                ifs.ignore(256,' ' );
                ifs >> NbLine;
            }
            
            else if(LigneCour == "NbCol:")
            {
                ifs.ignore(256,' ' );
                ifs >> NbColone;
            }
            else if(LigneCour == "Bot:")
            {
                ifs.ignore(256, ' ');
                ifs >> Botstr;
                
                if(Botstr == "true")
                    Bot = true;
                else
                    Bot = false;
            }
            else if(LigneCour == "Difficulty:")
            {
                ifs.ignore(256, ' ');
                ifs >> Difficulty;
            }
            else if(LigneCour == "XPosPlay1:")
            {
                ifs.ignore(256,' ' );
                ifs >> Player1.first;
            }
            else if(LigneCour == "YPosPlay1:")
            {
                ifs.ignore(256,' ' );
                ifs >> Player1.second;
            }
            else if(LigneCour == "XPosPlay2:")
            {
                ifs.ignore(256,' ' );
                ifs >> Player2.first;
            }
            else if(LigneCour == "YPosPlay2:")
            {
                ifs.ignore(256,' ' );
                ifs >> Player2.second;
            }
            else if(LigneCour == "XPosObs:")
            {
                ifs.ignore(256, ' ');
                ObsPos.resize(ObsPos.size() + 1);
                ifs >> ObsPos[CptObs].first;
            }
            else if(LigneCour == "YPosObs:")
            {
                ifs.ignore(256, ' ');
                ifs >> ObsPos[CptObs].second;
                ++CptObs;
            }
            
        }
        InitMat(Mat, unsigned(NbLine), unsigned(NbColone), Player1, Player2);
        
        
        if(ObsPos.size() > 0)
        {
            for(CPosition Pos : ObsPos)
            {
                Mat[Pos.first][Pos.second] = KTokenObstacle;
            }
        }
        
        Mat[Player1.first][Player1.second] = KTokenPlayer1;
        Mat[Player2.first][Player2.second] = KTokenPlayer2;

        
    }//InitMatFromFile()

    /*!
    @fn InitMatrixSize()
    @brief This function ask to the user the size of the matrix.
    @return CPositon This contain the lines and colomuns of the matrix.
    */
    CPosition InitMatrixSize()
    {   
        CPosition LineCol;
         cout << "\nType the size of the play area : ";
        while(true)
        {
           while(LineCol.first < 10) 
           {
			   cout << "\nType the number of lines : "; //we ask the numbers of line for the matrix
				 
				cin >> LineCol.first;

				if(cin.eof()) break; //we check the input.
				else if(cin.fail())
				{
					cin.clear();
					cin.ignore( numeric_limits<streamsize>::max(), '\n');
					continue;
				}
				else if(LineCol.first < 10) // Check if the numbers of line if good.
				{
					cout << "Type a size above 10\n";
					continue;
				} 	
            }
            while(LineCol.second < 10)
            { 
				cout << "\nType the number of column : "; //we ask the numbers of column for the matrix
				cin >> LineCol.second;

				if(cin.eof()) break;
				else if(cin.fail())
				{
					cin.clear();
					cin.ignore( numeric_limits<streamsize>::max(), '\n');
					continue;
				}
				else if(LineCol.second < 10)// Check if the size id good.
				{
					cout << "Type a size above 10\n"; 
					continue;
				}
			}
            break;
            
        }       
        return LineCol;
    }//InitMatrixSize()

    /*!
      @fn void MultiPlayers(bool& Bot)
      @brief This function ask to the player if he wants to play against the computer (IA or Bot)
      or against other player.
      @param[out] Bot This is a boolean which is true when the bot is activated otherwise it's false
    */
    void MultiPlayers(bool& Bot)
    {
        char Answer;
        while(true)
        {
            cout << "\nDo you want to play with the IA ? (y or n) : \n";
            cin >> Answer; //Ask the choice of the of the user.
            if(cin.eof()) break;//Check the input.
            else if(isdigit(Answer)) continue;

            Answer = tolower(Answer);
            
            if(Answer == 'y') // If the answer is 'y' we activat the bot
            {
                Bot = true;
                break;
            }
            else if (Answer == 'n')// Else no.
            {
                Bot = false;
                break;
            }
            else // Or if an error is encountered.
            {
				cout << "Input error !";
                cin.ignore(numeric_limits<streamsize>::max(), '\n');
                continue;
            }
        }
    }//Multiplayer()

    /*!
    @fn  void ReadF(CMatrix& Mat,const string & File,CPosition& Pos,bool Player,unsigned Number =0)
    @param[out] Mat This is the matrix where we want to work.
    @param[in] File This is the name of file we want to read.
    @param[in] Pos This is the position we want to modify.
    @param[in] Player Is to know if this function is used for a player or not.
    @param[in] Number Is to know if it's for a player and for what player.
    @brief This function read the file place in parameter and modify the matrix to adapt the Matrix at this file.
    */
    void ReadF(CMatrix& Mat,const string & File,CPosition& Pos,bool Player,unsigned Number =0)
    {
        ifstream ifs(File, ios_base::in);//Same like before
        
        if(!ifs.is_open())
        {
                cout << "No save file detected \n";
                cout << "Press any key to quit";
                getch();
                return;
        }
        
        CVLine::size_type NbLine = Mat[0].size();
        CVLine::size_type NbColone = Mat.size();
        string LigneCour;
        
        while(ifs >> LigneCour) //we get line by line the content of the ifs.
        {
            if(LigneCour == "NbLine:")
            {
                ifs.ignore(256,' ' );
                ifs >> NbLine;
            }
            
            else if(LigneCour == "NbCol:")
            {
                ifs.ignore(256,' ' );
                ifs >> NbColone;
            }
            if(Player == false) // if is not use for a player we execute the following instructions
            {
                if(LigneCour == "XPosObs:")
                {
                    ifs.ignore(256, ' ');
                    ifs >> Pos.first;
                }
                else if(LigneCour == "YPosObs:")
                {
                    ifs.ignore(256, ' ');
                    ifs >> Pos.second;
                }
                else
                {
                    Pos.first = 0;
                    Pos.second = 0;
                }
            }
            else
            {
                if(Number == 1) // If the function is call for a player, and for the first the folliwinf instructions are execute.
                {
                    if(LigneCour == "XPosPlay1:")
                    {
                        ifs.ignore(256, ' ');
                        ifs >> Pos.first;
                    }
                    else if(LigneCour == "YPosPlay1:")
                    {
                        ifs.ignore(256, ' ');
                        ifs >> Pos.second;
                    
                    }
                }
                else //Else this is for the Player 2
                {
                    if(LigneCour == "XPosPlay2:")
                    {
                        ifs.ignore(256, ' ');
                        ifs >> Pos.first;
                    }
                    else if(LigneCour == "YPosPlay2:")
                    {
                        ifs.ignore(256, ' ');
                        ifs >> Pos.second;
                    
                    }
                }
            }
        }
        
    }//ReadF()

    /*!
    @fn void EditMap(CMatrix& Mat,string File,bool Exist = false)
    @param[in] Mat This parameter is to modify the matrix place here.
    @param[in] File This is the name of file in which the user  wants to save the current matrix.
    @param[in] exist This paramater is for know if the file already exit and if already exit, it recup the data in file.
    @brief Thif function is use for creat or charge a matrix and modify it to creat your own map and after you can use it to play on it.
    */
    void EditMap(CMatrix& Mat,string File,bool Exist = false)
    {
        unsigned Choice = 0;
        
        CVLine::size_type NbLine = Mat[0].size();
        CVLine::size_type NbColumn = Mat.size();
        
        unsigned Pres1 = 0; //Initalaze if the Player 1 position is placed.
        unsigned Pres2 = 0; //Initalaze if the Player 2 position is placed.
        
        if(!Exist) // If the file doesn't already exist we save in first the numbers of lines and columns.
        {
            SaveFirst(File, unsigned(NbLine), unsigned(NbColumn));
        }
        else // Else we turn at 1 the Presence var, for the playr one and two. 
        {
            Pres1 = 1;
            Pres2 = 1;
        }
        
        unsigned Allow; // A var to know if the move is allow or not.

        cout <<"\nInstructions : \nFor move is the same way like in game (press s for return to menu) press any key to start\n";
        getch();
            while (true)
            {
                Choice = menu(); // Ask the choice of the player.
                
                if (Choice == 1) //If the choice is 1 it places an obstacle.
                {
                    CPosition Pos;
                    ReadF(Mat, File, Pos,false); // we read the file to know if obstacle is already placed.
                    CPosition Pos2;
                    Pos2.first = 0;
                    Pos2.second = 0;
                    
                    Mat[Pos.first][Pos.second] = KTokenObstacle;
                    Mat[Pos2.first][Pos2.second] = KTokenObstacle;
                    ShowMatrix(Mat);
                    char Move;
                    do // A loop to place the current object and escape when s is pressed
                    {
                        Move = getch();
                        Move = tolower(Move);
                        MoveToken(Mat,Move, Allow,Pos2,Touches,'y');
                        ShowMatrix(Mat);
                        if(Allow == 1)
                            cout << "Impossible move\n";   
                    }while(Move != 's');
                    continue;
                }
                else if(Choice == 2)
                {
                    CPosition Pos2;
                    ReadF(Mat,File,Pos2,true,1); // Read the file contains a value for the player.
                    Mat[Pos2.first][Pos2.second] = KTokenPlayer1;
                    ShowMatrix(Mat);
                    char Move;
                    do // Like before
                    {
                        Move = getch();
                        Move = tolower(Move);
                        MoveToken(Mat,Move, Allow,Pos2,Touches,'y');
                        ShowMatrix(Mat);
                        if(Allow == 1)
                            cout << "Impossible move\n";   
                    }while(Move != 's');
                    Pres1=1;
                    continue;
                }
                else if(Choice == 3)
                {
                    CPosition Pos2;
                    ReadF(Mat,File,Pos2,true);
                    
                    Mat[Pos2.first][Pos2.second] = KTokenPlayer2;
                    ShowMatrix(Mat);
                    
                    char Move;
                    do
                    {
                        Move = getch();
                        Move = tolower(Move);
                        MoveToken(Mat,Move, Allow,Pos2,Touches,'y');
                        ShowMatrix(Mat);
                        if(Allow == 1)
                            cout << "Impossible move\n";   
                    }while(Move != 's');
                    Pres2=1;
                    continue;

                }
                else if(Choice == 4)
                {
                    if (Pres1 != 0 && Pres2 !=0) // check if the Player 1 and 2 are placed
                    {
                        SaveFirst(File, unsigned(NbLine), unsigned(NbColumn));
                        SaveEd(Mat, File);
                        break;
                    }
                    else
                    {
                        cout << "\nInvalid map : the location of Player 1 or 2 is not defined \n";
                        continue;
                    }
                }
                else
                {
                    cout << "Make a valid choice\n";
                    continue;
                }
            
            }
    }//EditMap()

    /*!
    @fn void CheckName(const string& PlayerName, vector<string>& Score)
    @brief This function verify if the name of the player is in the 
    vector Score. In this case, the function implements one to the number inside the string. 
    If the name is not found, it adds a case with the name of the new player and a 1.
    @param[in] PlayerName This string contain the name of the player.
    @param[out] Score This is the vector which contains the scores of each players. 
    One case contain the name of the player and his number of victory separed by a space. 
    */
    void CheckName(const string& PlayerName, vector<string>& Score)
    {
        int Control=0;                        //This variable indicates if the Name is in the vector
        for (unsigned i = 0; i < Score.size(); ++i)           //We seek the Name in the vectore Score
        {
            string Curent;                      //This will contain the names
            unsigned j=0;
            while(Score[i][j] != ' ' && j < Score[i].size())  //Each case contain the name and the number of victory separed by a space, so we just keep the names.
            {
                Curent+=Score[i][j];
                ++j;
            }

            if(PlayerName == Curent)           //If the name is present, we implements one the value of the victory of the player
            {
               
               string StrScore;                //This will contain the number of victory of the player
               unsigned k = 0;
               while(Score[i][k] != ' ' &&  k < Score[i].size())             
               {
                   ++k;
               }
               for(unsigned r = k+1; r < Score[i].size(); ++r)
               {
                    StrScore += Score[i][r];
               }
               int IntScore = stoi(StrScore);              //We convert the number found in int
               ++IntScore;                               //The player won, so we implements the variable score
               string Need = to_string(IntScore);        //We convert back the int into a string
               string Inject = PlayerName + " " + Need;         //We create a string which contain the name of the player then a space and then the new nomber of victory of the player
               Score[i] = Inject;                     //We replace the old string by the new one
               Control = 1;                           //The Name is present so we don't need to enter in the next if.
            }
        }

        if (Control == 0)                             //If the player is not in the vector Score we create a new case with is name and one victory
        {
            string Need = to_string(1);               //We convert 1 in string
            string Inject = PlayerName + " " + Need;           //We create the string that will contain the name of the player then a space and then the new nomber of victory, one in this case
            Score.push_back(Inject);
            
        }
    
    }//CheckName()

    /*!
    @fn void ReadScore(vector<string>& Score)
    @brief This function open the file score.txt, put the player's name and the number of his victory, 
    as one string in the vector Score. It also sorts the players by order of victory.
    @param[out] Score This is the vector which contains the scores of each players. 
    One case contain the name of the player and his number of victory separed by a space. 
    */
    void ReadScore(vector<string>& Score)
    {
        std::ifstream ifs("score.txt",ios_base::in);         //we open the file score.txt which contain the names of the players and their number of victory
        
        if(!ifs.is_open())
        {

            cout << "Score file not found or access denied\n";
            cout << "The file has been created\n";
            ofstream ofs("score.txt");
            return;
        }

        string Name;                              //this value will contain the extraction of the IFS
         
        while(!ifs.eof())
        {
            getline(ifs,Name);  
            Score.push_back(Name);                //we exctract each line of the IFS and store them into a vector
        }

        Score.erase(Score.end()-1);

        vector<string> Result; //This is temporary variable use for sort the score.
        Result.resize(Score.size());
        
        for(unsigned i = 0; i < Score.size(); ++i)
        {
            unsigned j = 0;
                
            while(Score[i][j] != ' ')
            {
                ++j;
            }
            for(unsigned k = j+1; k < Score[i].size(); ++k)
            {
                if(k != Score[i].size()-1)
                {
                    Result[i] = Score[i][k]; //Extraction of the name.
                    Result[i] += Score[i][k+1]; //Extraction of the score.
                    ++k;
                }
                else
                {
                    Result[i] = Score[i][k];
                }
            }   
        }
        
        //Sort of the results
        
        for(unsigned l = 0; l < Score.size(); ++l)
        {
            for(unsigned m = 0; m < Score.size(); ++m)
            { 
                int First = atoi(Result[l].c_str());
                int Second = atoi(Result[m].c_str());
                if(First > Second)
                {
                    swap(Score[l], Score[m]);
                    swap(Result[l], Result[m]);
                }
                
            }
        }
        
        
    }//ReadScore()

    /*!
    @fn void RecordScore (vector<string>& Score)
    @brief This function write the vector Score in the file score.txt.
    @param[out] Score This is the vector which contains the scores of each players. 
    One case contain the name of the player and his number of victory separed by a space. 
    */
    void RecordScore (vector<string>& Score)
    {
        ofstream ofs;
        ofs.open("score.txt",ios::out|ios::trunc);
        
        if(!ofs.is_open())
        {

            cout << "Score file not found or access denied\n";
            return;
        }
        for (unsigned i = 0; i < Score.size(); ++i)
        {
           ofs << Score[i] << endl;
        }
        ofs.close();
        
    }//RecordScore()

    /*!
    @fn void ShowVec(std::vector<string> Score)
    @brief This function basicaly displays each case of the vector Score.
    @param[in] Score This is the vector which contains the scores of each players. 
    */
    void ShowVec(std::vector<string> Score)
    {
        for(unsigned m = 0; m < Score.size();++m)
        {
            cout << Score[m] << endl;
        }
        getch();
                
        
    }//ShowVec()
    
    /*!
    @fn string CheckBonus (CMatrix& Matrix, CPosition& Player,vector<char> Touche =Touches)
    @brief This function checks all the positions arround the player for the presence of bonus.
    If a bonus is found, its position is stored in the string StringPositionBonus. This string is returned.
    @param[in] Matrix This is the Matrix of the game
    @param[in] Player This is the position of the Player
    @param[in] Touches This vector contain the direction of the keys
    */ 
    string CheckBonus (CMatrix Matrix, CPosition Player,vector<char> Touche = Touches)
    {
        string StringPositionBonus;        //This will contain the position of the bonuses arround the player
        StringPositionBonus = " ";    // We make sure the string to be empty

        if (Player.first != 0)
        {
            if(Matrix[Player.first-1][Player.second] == KTokenBonusTourDouble)
                StringPositionBonus = StringPositionBonus + Touche[1];
        }
        if (Player.second != 0)
        {
            if(Matrix[Player.first][Player.second-1] == KTokenBonusTourDouble)
                StringPositionBonus = StringPositionBonus + Touche[3];
        }
        if (Player.first != Matrix[0].size()-1)
        {
             if(Matrix[Player.first+1][Player.second] == KTokenBonusTourDouble)
                StringPositionBonus = StringPositionBonus + Touche[6];
        }
        if (Player.second != Matrix.size()-1)
        {
            if(Matrix[Player.first][Player.second+1] == KTokenBonusTourDouble)
                StringPositionBonus = StringPositionBonus + Touche[4];
        }
        if(Player.first != 0 && Player.second != Matrix.size()-1)
        {
            if(Matrix[Player.first-1][Player.second+1] == KTokenBonusTourDouble)
                StringPositionBonus = StringPositionBonus + Touche[2];
        }
        if(Player.first != 0 && Player.second != 0)
        {
            if (Matrix[Player.first-1][Player.second-1] == KTokenBonusTourDouble)
                StringPositionBonus = StringPositionBonus + Touche[0];
        }
        if(Player.first != Matrix[0].size()-1 && Player.second != 0)
        {
            if(Matrix[Player.first+1][Player.second-1] == KTokenBonusTourDouble)
                StringPositionBonus = StringPositionBonus + Touche[5];
        }
        if(Player.first != Matrix[0].size()-1 && Player.second != Matrix.size()-1)
        {
            if(Matrix[Player.first+1][Player.second+1] == KTokenBonusTourDouble)
                StringPositionBonus = StringPositionBonus + Touche[7];
        }
        return StringPositionBonus;

    }//checkBonus()

    /*!
    @fn Restart()
    @brief This function is only used to propose at user to retry the game or not.
    */
    char Restart()
    {
        char Restart;
        while(true)
        {
            cout << "Do you want to retry ? (y or n) : ";
            cin >> Restart;
            if (cin.eof()) break;
            else if (cin.fail()) {
                cin.clear();
                cin.ignore( numeric_limits<streamsize>::max(), '\n');
                continue;
            }
            Restart = tolower(Restart);
            break;
        }
        return Restart;
    }//Restart()

    /*!
    @fn void Jeu()
    @breif It's the main function of the game, that launches everything which is the actual game. Like if
    the user want to load a game or create a new game with all option.
    */
    void Jeu()
    {
        char Replay = 'y';
        
        while(Replay != 'n')
        {  
            CMatrix Matrice;
            CMatrix EmptyMat;
            CPosition Player1;
            CPosition Player2;
            
            bool Bot;// Declare IA
            bool Virgin = false;
            unsigned Difficulty;

            vector<string> TabName;

            std::string strBonusTourDouble;
            
            vector <CPosition> ObsPos;

            ClearScreen();

            cout << "Load a file or Play ? (l/p) : ";
            char Choix;
            cin >> Choix;
            Choix = tolower(Choix);
            
            if(cin.eof()) break;
            control();
            
            if(Choix == 'l')
            {
                string Filex;
                
                LireLog(Filex);
                InitMatFromFile(Matrice, Filex, Player1, Player2, ObsPos, Bot, Difficulty);

                if(Matrice.size() < 1)
                {
                    cout << "\nConfig file loaded\n";
                    string FileName = "config.txt";
                    InitMatFromFile(Matrice, FileName, Player1, Player2, ObsPos, Bot, Difficulty);
                }
            }
            else if(Choix == 'p')
            {
                unsigned NombreLine;
                unsigned NombreCol;
                CPosition LineColNb;
                
                LineColNb = InitMatrixSize();
                
                if (cin.eof()) 
					break;
					
                NombreLine = LineColNb.first;
                NombreCol = LineColNb.second;
                
                Player1.first = 0;
                Player1.second = NombreCol-1;
                Player2.first = NombreLine-1;
                Player2.second = 0;
                
                InitMat(Matrice, NombreLine, NombreCol, Player1, Player2, Virgin);
            }
            else
            {
                cout << "\nInvalid choice\n";
                cin.clear();
                cin.ignore( numeric_limits<streamsize>::max(), '\n');
                continue;
            }
            DifficultyChoice(Matrice, Difficulty);
            MultiPlayers(Bot);
            
            unsigned Cpt = 0;
            unsigned NbrCoups = ( (Matrice.size() * Matrice[0].size()) / 2); //Initialize the number of movement that the players, according to the size of the matrix, can make
            
            ShowMatrix(Matrice);
            
            char Move;
            unsigned Num = 1;
            
            while(Player1 != Player2)
            {
                //The variable Success permit to show the message "Mouvement impossible" when it's impossible to move
                unsigned Success;

                if (cin.eof()) 
                {
                    Save(Matrice, "Save.txt", Bot, Difficulty);
                    EcriLog("Save.txt");
                    return;
                }
                
                if(!Bot || (Bot && Num == 1))
                    {
                        //if there is no bot or if there is a bot and if it's the first player's move we ask him to move
                        cout << "\nPlayer " << Num << " make a move  : "
                        << '\n';
                        Move = getch();
                        if (Move == Touches[8]) 
                        {
                            Save(Matrice, "Save.txt", Bot, Difficulty);
                            EcriLog("Save.txt");
                            return;
                        }
                    }

                if(Num == 1)
                {
                    strBonusTourDouble = " ";
                    strBonusTourDouble = CheckBonus(Matrice, Player1);
                    MoveToken(Matrice, Move, Success, Player1);
                    ShowMatrix(Matrice);
                    
                    for (unsigned i = 0; i < strBonusTourDouble.size(); ++i)
                    {
                        if(strBonusTourDouble[i] == Move)
                        {
                            cout << "You have won a bonus movement : \n";
                            Move = getch();
                            MoveToken(Matrice, Move, Success, Player1);
                            break;
                        }
                    }
                    if(NbrCoups == 0)
                        break;// break when the number of authorized movement is reached
                    Num = 2;
                }
                else
                {
                    if(!Bot)
                    {
                        strBonusTourDouble = " ";
                        strBonusTourDouble = CheckBonus(Matrice, Player2);
                        MoveToken(Matrice, Move, Success, Player2);
                        ShowMatrix(Matrice);
                        
                        for (unsigned i = 0; i < strBonusTourDouble.size(); ++i)
                        {
                            if(strBonusTourDouble[i] == Move)
                            {
                                cout << "You have a won a bonus movement : \n";
                                Move = getch();
                                MoveToken(Matrice, Move, Success, Player2);
                                break;
                            }
                        }
                        
                        if(NbrCoups == 0)
                            break;
                    }
                    else
                        IA(Matrice, Player1, Player2, Difficulty);
                    Num = 1;
                }
                if (Success == 1)
                {
                  ShowMatrix(Matrice);
                  cout << "Impossible movement\n";  
                }
                else
                {
                    ShowMatrix(Matrice);
                }
                ++Cpt;
                --NbrCoups;
                cout << "There are "<< NbrCoups << " move to make \n";
                
            }
            remove("Save.txt");
            
            if(NbrCoups > 0)
            {
                if(Num == 1)
                {
                    Num = 2;
                }
                else
                {
                    Num = 1;
                }
                cout << "The player " << Num << " won ! \n";    
            }//If the End of the timer is not reached, choice of the winner
            else
            {
                cout << "There is no winner ! \n";
                Replay = Restart();
                if (cin.eof()) break;
                continue;
            }// else draw

            ReadScore(TabName);

            if (Bot && Num == 2)      //If the bot win we just show the scores
            {
                cout << "Here the highscore \n";
                
                ShowVec(TabName);
                CheckName("Bot", TabName);
                RecordScore(TabName);
                Replay = Restart();
                if (cin.eof()) break;
                
            }
            else                   //If the player wins we ask him his name
            {
                string Name; 
                
				while(Name.size() == 0)
				{
					cout << "Please enter your name (without spaces) \n";              
					cin >> Name;
					control();// check if the buffer is valid or not
					if(cin.eof()) break;	
					
				}
				cin.ignore( numeric_limits<streamsize>::max(), '\n');
                CheckName(Name,TabName);
                cout << "Here the highscore \n";
                ShowVec(TabName);
                RecordScore(TabName);
                Replay =  Restart();
                
                if (cin.eof()) 
					break;
            }    
        }
        
    }//Jeu()

    /*!
    @fn int TestTouche(const char& newtouch,vector<char> tab)
    @param[in] NewTouch It the new touch of the user want to attribut.
    @param[in] Tab This parameter contain the touch alreday used by all mouvement.
    @brief 
    */
    int TestTouche(const char& NewTouch,vector<char> Tab)
    {
        for (unsigned i = 0; i < Tab.size(); ++i) 
        {
            if (NewTouch == Tab[i])
            {
                return 1;
            }
        }
        return 0;
    }//TestTouche

    /*!
    @fn Option
    @brief This function is use to change the touch to personalize the enviroment of the player.
    */
    void Option()
    {
        unsigned Choice;
        ClearScreen();
        while(Choice != 10)
        {

            cout << "What key do you want to reasign :";
            cout << "\n1.Diagonal Up Left";
            cout << "\n2.Up";
            cout << "\n3.Diagonal Up Right";
            cout << "\n4.Left";
            cout << "\n5.Right";
            cout << "\n6.Diagonal Bottom Left";
            cout << "\n7.Bottom";
            cout << "\n8.Diagonal Bottom Right";
            cout << "\n9.Save";
            cout << "\n10.Quit";
            cout << "\nYour choice : ";
            
            cin >> Choice;

            if (cin.eof())
            {
                break;
            }

            if(cin.fail() || (Choice < 1 && Choice > 9) )
            {
                cin.clear();
                cin.ignore( numeric_limits<streamsize>::max(), '\n');
                cout << '\n';
                
                continue;
            }
            cin.ignore( numeric_limits<streamsize>::max(), '\n');
            string Invite = "\nNew key : ";
            char CarTemp;
            if(Choice == 1)
            {
                int Temp = 1;
                while (Temp !=0)
                {
                    cout << "\nActual key : " << Touches[0] << Invite;
                    cin >> CarTemp;
                    control();
                    Temp = TestTouche(CarTemp,Touches);
                }
                Touches[Choice-1] = CarTemp;
                continue;
            }
            else if(Choice == 2)
            {
                int Temp = 1;
                while (Temp !=0)
                {
                    cout << "\nActual key : " << Touches[1] << Invite;
                    cin >> CarTemp;
                    control();
                    Temp = TestTouche(CarTemp,Touches);
                }
                Touches[Choice-1] = CarTemp;
                continue;
            }
            else if(Choice == 3)
            {
                int Temp = 1;
                while (Temp !=0)
                {
                    cout << "\nActual key : " << Touches[2] << Invite;
                    cin >> CarTemp;
                    control();
                    Temp = TestTouche(CarTemp,Touches);
                }
                Touches[Choice-1] = CarTemp;
                continue;
            }
            else if(Choice == 4)
            {
                int Temp = 1;
                while (Temp !=0)
                {
                    cout << "\nActual key : " << Touches[3] << Invite;
                    cin >> CarTemp;
                    control();
                    Temp = TestTouche(CarTemp,Touches);
                }
                Touches[Choice-1] = CarTemp;
                continue;
            }
            else if(Choice == 5)
            {
                int Temp = 1;
                while (Temp !=0)
                {
                    cout << "\nActual key : " << Touches[4] << Invite;
                    cin >> CarTemp;
                    control();
                    Temp = TestTouche(CarTemp,Touches);
                }
                Touches[Choice-1] = CarTemp;
                continue;
            }
            else if(Choice == 6)
            {
                int Temp = 1;
                while (Temp !=0)
                {
                    cout << "\nActual key : " << Touches[5] << Invite;
                    cin >> CarTemp;
                    control();
                    Temp = TestTouche(CarTemp,Touches);
                }
                Touches[Choice-1] = CarTemp;
                continue;
            }
            else if(Choice == 7)
            {
                int Temp = 1;
                while (Temp !=0)
                {
                    cout << "\nActual key : " << Touches[6] << Invite;
                    cin >> CarTemp;
                    control();
                    Temp = TestTouche(CarTemp,Touches);
                }
                Touches[Choice-1] = CarTemp;
                continue;
            }
            else if(Choice == 8)
            {
                int Temp = 1;
                while (Temp !=0)
                {
                    cout << "\nActual key : " << Touches[7] << Invite;
                    cin >> CarTemp;
                    control();
                    Temp = TestTouche(CarTemp,Touches);
                }
                Touches[Choice-1] = CarTemp;
                continue;
            }
            else if(Choice == 9)
            {
                int Temp = 1;
                while (Temp !=0)
                {
                    cout << "\nActual key : " << Touches[8] << Invite;
                    cin >> CarTemp;
                    control();
                    Temp = TestTouche(CarTemp,Touches);
                }
                Touches[Choice-1] = CarTemp;
                continue;
            }
            else if(Choice == 10)
            {
                break;
            }
            else
            {
                cout << "Impossible\n";
                cout << "Make a valid choice \n";
                continue;
            }
        }
    }//Option()

    /*!
    @fn InitMatFromFileForEdit (CMatrix & Mat, const string & FileName)
    @param [out] Mat This is the name of the matrix we want to initialaze.
    @param [in] FileName This is the name of file we want to use to initialise the matrix.
    */
    void InitMatFromFileForEdit (CMatrix & Mat, const string & FileName)
    {
        bool Virgin = false;
        CPosition Player1 = make_pair(0,0);
        CPosition Player2 = make_pair(0,0);
        vector<CPosition> ObsPos;
        
        ifstream ifs(FileName);
        
        if(!ifs.is_open())
        {
            if(FileName == "Save.txt")
            {
                cout << "Save file not found !\n";
            }
            else
            {
                cout << "Configuration file not found !\n";
            }
            return;
        }
        
        CVLine::size_type NbLine;
        CVLine::size_type NbColone;

        string LigneCour;
        unsigned CptObs = 0;
        
        while(ifs >> LigneCour)
        {
            if(LigneCour == "NbLine:")
            {
                ifs.ignore(256,' ');
                ifs >> NbLine;
            }
            
            else if(LigneCour == "NbCol:")
            {
                ifs.ignore(256,' ' );
                ifs >> NbColone;
            }
            else if(LigneCour == "XPosPlay1:")
            {
                ifs.ignore(256,' ' );
                ifs >> Player1.first;
            }
            else if(LigneCour == "YPosPlay1:")
            {
                ifs.ignore(256,' ' );
                ifs >> Player1.second;
            }
            else if(LigneCour == "XPosPlay2:")
            {
                ifs.ignore(256,' ' );
                ifs >> Player2.first;
            }
            else if(LigneCour == "YPosPlay2:")
            {
                ifs.ignore(256,' ' );
                ifs >> Player2.second;
            }
            else if(LigneCour == "XPosObs:")
            {
                ifs.ignore(256, ' ');
                ObsPos.resize(ObsPos.size() + 1);
                ifs >> ObsPos[CptObs].first;
            }
            else if(LigneCour == "YPosObs:")
            {
                ifs.ignore(256, ' ');
                ifs >> ObsPos[CptObs].second;
                ++CptObs;
            }
            
        }

        InitMat(Mat, unsigned(NbLine), unsigned(NbColone),Player1, Player2, Virgin);

        
        if(ObsPos.size() > 0)
        {
            for(CPosition Pos : ObsPos)
            {
                Mat[Pos.first][Pos.second] = KTokenObstacle;
            }
        }

        Mat[Player1.first][Player1.second] = KTokenPlayer1;
        Mat[Player2.first][Player2.second] = KTokenPlayer2;
        
    }//nitMatFromFileForEdit()
    /*!
    @fn int Run()
    @return int This int says if the function went well if the value of @n int is 0 or not. 
    @brief This function launch a menu, and accordind to the choice of the user, launch the the function
    corresponding to the user's choice.
    */ 
    int Run()
    {
        bool Virgin = true;// Is the variable who determine if the map is virgin or not
        CPosition Player1;
        CPosition Player2;
        
        
        while(true)
        {
                ClearScreen();
                cout << "Welcome to \"Catch me if you can\"\n"
					 << "by Anthony.L, Thomas.M, Loïc.P, Dylan.P \n\n"
					 << "1.Play\n"
					 << "2.Launch the map editor\n"
					 << "3.Launch option\n"
					 << "4.Exit\n"
					 << "What is your choice ? ";

                unsigned Choix;
                cin >> Choix;

                if(cin.eof()) break;
                if(Choix < 1 || Choix > 4)
                {
                    cin.clear();
                    cin.ignore( numeric_limits<streamsize>::max(), '\n');// Avoid having problems with the buffer
                    continue;
                }

                CMatrix Matrix;

                if(Choix == 1)
                   {
                        cin.ignore(numeric_limits<streamsize>::max(), '\n');
                        Jeu();
                        break;
                   }
                else if(Choix == 2)
                    {
                        while (true)
                        {
                            cin.ignore(numeric_limits<streamsize>::max(), '\n');
                            cout << "\nNew file(n) or existing file(e) : ";
                            
                            char Resp;
                            cin >> Resp;
                            if(cin.eof()) break;

                            Resp = tolower(Resp);

                            string File;

                            if(Resp == 'n')
                            {
                                CPosition Colline;
                                
                                
                                cout << "\nEnter the filename : ";
                                
                                cin >> File;
                                if(cin.eof()) break;
                                control();
                                EcriLog(File);
                                
                                string fic(File);
                                Colline = InitMatrixSize();
                                
                                CMatrix Matrix;
                                unsigned Line = Colline.first;
                                unsigned Col = Colline.second;
                                
                                InitMat(Matrix, Line, Col,Player1, Player2, Virgin);
                                EditMap(Matrix,fic);
                               
                                break;
                            }
                            else if(Resp == 'e')
                            {
                                cin.ignore(numeric_limits<streamsize>::max(), '\n');

                                CMatrix Matrix;
                                string Temp;
                                
                                LireLog(Temp);
                                InitMatFromFileForEdit(Matrix,Temp);
                                if(Matrix.size() > 1)
									EditMap(Matrix,Temp,true);
                                
                                break;
                            }
                            else
                            {
                                cout << "\nPlease, make a valid choice, press a key to continue";
                                cin.clear();// remove the error bits
                                cin.ignore( numeric_limits<streamsize>::max(), '\n');
                                continue;
                            }
                        }
                        
                    }
                    
                   else if(Choix == 3)
                    {
                        cin.ignore(numeric_limits<streamsize>::max(), '\n');
                        Option();
                        continue;
                    }
                   else if(Choix == 4)
					{	
						cin.ignore(numeric_limits<streamsize>::max(), '\n');
						cout << "Thank you and goodbye !\n";
						break;
					}
                   else
                    {
                        cout << "Please, make a valid choice\n";
                        cin.ignore(numeric_limits<streamsize>::max(), '\n');
                        continue;
                    }
            }
			return 0;
        }//Run()

    }//NsCatch

int main()
{
    NSCatch::Run();
    return 0;
}
